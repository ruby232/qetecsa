
#include "qetecsa.h"


QEtecsa::QEtecsa(QWidget *parent)
    : QWidget(parent)
{
    QString database_dir;
    QFile file(qApp->applicationDirPath() + "/data.dat");
    file.open(QIODevice::ReadOnly);

    QDataStream in(&file);
    in >> database_dir;

    establecerEstilos();

    this->database = database_dir;
    this->movil = true;
    this->status = true;
    w_modal = new Modal(this);
    connect(w_modal,SIGNAL(cancel()),this,SLOT(cancel()));
    setupUi(this);

    emit statusMessage(tr("Ready."));

    this->thread = new QueryThread(this->get_query(),this->database,this);
    connect(thread, SIGNAL(result(QSqlQueryModel*,double)), this, SLOT(resulQuery(QSqlQueryModel*,double)));
    connect(thread,SIGNAL(error(QString)),this,SLOT(error(QString)));

    this->execQuery();
}

QEtecsa::~QEtecsa()
{
}

void QEtecsa::execQuery()
{
    if (this->status) {
        thread->setQuery(this->get_query());
        thread->start();
        w_modal->setMessage(this->get_message());
        w_modal->show();
    }

}

void QEtecsa::resulQuery(QSqlQueryModel* model, double time)
{
    w_modal->hide();

    table->setModel(model);

    table->setColumnWidth(0,100);
    table->setColumnWidth(1,300);

    if(this->movil){
        table->setColumnWidth(2,100);
    }

    table->horizontalHeader()->setStretchLastSection(true);

    if (model->lastError().type() != QSqlError::NoError)
        emit statusMessage(model->lastError().text());
    else if (model->query().isSelect())
        emit statusMessage(tr("Query OK."));
    else
        emit statusMessage(tr("Query OK, number of affected rows: %1").arg(
                               model->query().numRowsAffected()));
}

void QEtecsa::error(QString error)
{
    QMessageBox::critical(this,"Error",error);
    status = false;
}

void QEtecsa::cancel()
{
    thread->quit();
    w_modal->hide();
}

QString QEtecsa::get_query(){


    QString query = "select number as Número,name as Nombre,address as Direccón ";

    if(this->movil){
        query = "select number as Número,name as Nombre,identification as CI,"
                "address as Direccón from movil ";
    }else{
        query += " from fix ";
    }

    if(!this->le_numero->text().isEmpty()){
        query += " where number ";

        QString operador = "";
        QString numero = "53" + this->le_numero->text().trimmed();
        switch (this->cb_numero_tipo->currentIndex()) {
        case 0://Igual
            operador = " = '%1' ";
            break;
        case 1://Comienza con
            operador = " LIKE '%%1' ";
            break;
        case 2://Termina con
            operador = " LIKE '%1%' ";
            numero = this->le_numero->text().trimmed();
            break;
        default:
            operador = " LIKE '%%1%' ";
            numero = this->le_numero->text().trimmed();
            break;
        }

        query += QString(operador).arg(numero);
    }

    if(!this->le_nombre->text().isEmpty()){
        if(!this->le_numero->text().isEmpty()){

            if(this->cb_numero_nombre->currentIndex() == 1){
                query += " and ";
            }else{
                query += " or ";
            }
        }else{
            query += " where ";
        }
        query += QString(" name like '%%1%' ").arg(this->le_nombre->text());
    }

    query += ";";
    qDebug() << query;
    return query;
}

QString QEtecsa::get_message(){


    QString message = "Buscando teléfono ";

    if(this->movil){
        message += " movil ";
    }else{
        message += " fijo ";
    }

    if(!this->le_numero->text().isEmpty()){
        message += " con número ";

        QString operador = "";
        QString numero = "53" + this->le_numero->text().trimmed();
        switch (this->cb_numero_tipo->currentIndex()) {
        case 0://Igual
            operador = " Igual a ";
            break;
        case 1://Comienza con
            operador = " que Comienza con ";
            break;
        case 2://Termina con
            operador = " que Termina con ";
            numero = this->le_numero->text().trimmed();
            break;
        default:
            operador = " que contenga ";
            numero = this->le_numero->text().trimmed();
            break;
        }

        message += operador  + numero;
    }

    if(!this->le_nombre->text().isEmpty()){
        if(!this->le_numero->text().isEmpty()){

            if(this->cb_numero_nombre->currentIndex() == 1){
                message += " y ";
            }else{
                message += " o ";
            }
        }
        message += " contenga el Nombre " + this->le_nombre->text();
    }

    message += ".";
    qDebug() << message;
    return message;
}

void QEtecsa::on_cb_movil_clicked(bool checked)
{
    if(checked){
        this->movil = true;
    }else{
        this->movil = false;
    }
}


void QEtecsa::about()
{
    QMessageBox::about(this, tr("About"), tr("The SQL Browser demonstration "
                                             "shows how a data browser can be used to visualize the results of SQL"
                                             "statements on a live database"));
}

void QEtecsa::show_setting()
{
    WSettings *settigs = new WSettings(this);
    settigs->show();
}

void QEtecsa::on_submitButton_clicked()
{
    execQuery();
}

void QEtecsa::on_cb_numero_tipo_currentIndexChanged(int index)
{
    if(index == 1 || index == 0)
    {
        this->l_53->setVisible(true);
    }else{
        this->l_53->setVisible(false);
    }
}


void QEtecsa::establecerEstilos()
{
    //establecer estilo
    //qApp->setStyle(this->estilo);

    //establecesr estilo desde una hoja de estilo
    QFile file(qApp->applicationDirPath() +"/estilo.qss");
    file.open(QFile::ReadOnly);
    QString styleSheet = QLatin1String(file.readAll());
    file.close();

    qApp->setStyleSheet(styleSheet);
}
