#ifndef MODAL_H
#define MODAL_H

#include <QDialog>
#include <QLabel>
#include <QPushButton>
#include <QHBoxLayout>

class Modal : public QDialog
{
    Q_OBJECT

public:
    explicit Modal(QWidget *parent = 0);

    QString getMessage() const;
    void setMessage(const QString &value);

signals:
    void cancel();

private:
    QLabel *l_message;
    QPushButton *b_cancel;
    QString message;
};

#endif // MODAL_H
