#ifndef QUERYTHREAD_H
#define QUERYTHREAD_H

#include <QThread>
#include <QtSql>
#include <QSqlQueryModel>

class QueryThread : public QThread
{
    Q_OBJECT
public:
    QueryThread(QString query,QString db_dir,QObject *parent = 0);
    void run();

    
    QString getQuery() const;
    void setQuery(const QString &value);

signals:
    void result(QSqlQueryModel *result,double time);
    void error(QString error);
public slots:

private:
    QString query;
    QString db_dir;
    QSqlDatabase db;
    
};

#endif // QUERYTHREAD_H
