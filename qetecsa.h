#ifndef BROWSER_H
#define BROWSER_H

#include <QWidget>
#include <QSqlTableModel>
#include <QtWidgets>
#include <QHash>
#include <QDebug>

#include "ui_qetecsa_widget.h"
#include "querythread.h"
#include "modal.h"
#include "wsettings.h"

class ConnectionWidget;
QT_FORWARD_DECLARE_CLASS(QTableView)
QT_FORWARD_DECLARE_CLASS(QPushButton)
QT_FORWARD_DECLARE_CLASS(QTextEdit)
QT_FORWARD_DECLARE_CLASS(QSqlError)

class QEtecsa: public QWidget, private Ui::QEtecsa
{
    Q_OBJECT
public:
    QEtecsa(QWidget *parent = 0);
    virtual ~QEtecsa();


signals:
    void statusMessage(const QString &message);

private slots:    
    void execQuery();
    void resulQuery(QSqlQueryModel *model, double time);
    void error(QString error);
    void cancel();
    void about();
    void show_setting();

    void on_cb_movil_clicked(bool checked);
    void on_submitButton_clicked();
    void on_cb_numero_tipo_currentIndexChanged(int index);


private:
    bool movil;
    QString database;
    QueryThread *thread;
    bool status;
    Modal *w_modal;

    QString get_query();
    QString get_message();
     void establecerEstilos();
};

#endif
