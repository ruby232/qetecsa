#include <QtCore>
#include <QtWidgets>
#include <QtSql>

#include "qetecsa.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    QMainWindow mainWin;
    mainWin.setWindowTitle(QObject::tr("Etecsa 2014"));

    QEtecsa etecsa(&mainWin);
    mainWin.setCentralWidget(&etecsa);

    QMenu *fileMenu = mainWin.menuBar()->addMenu(QObject::tr("&File"));
    fileMenu->addSeparator();
    fileMenu->addAction(QObject::tr("&Settings"), &etecsa, SLOT(show_setting()));
    fileMenu->addAction(QObject::tr("&Quit"), &app, SLOT(quit()));

    QMenu *helpMenu = mainWin.menuBar()->addMenu(QObject::tr("&Help"));
    helpMenu->addAction(QObject::tr("About"), &etecsa, SLOT(about()));
    helpMenu->addAction(QObject::tr("About Qt"), qApp, SLOT(aboutQt()));

    QObject::connect(&etecsa, SIGNAL(statusMessage(QString)),
                     mainWin.statusBar(), SLOT(showMessage(QString)));

    mainWin.showMaximized();


    return app.exec();
}
