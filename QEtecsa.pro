TEMPLATE        = app
TARGET          = qetecsa

QT              += sql widgets

HEADERS         = \
    qetecsa.h \
    querythread.h \
    modal.h \
    wsettings.h
SOURCES         = main.cpp \
    qetecsa.cpp \
    querythread.cpp \
    modal.cpp \
    wsettings.cpp

FORMS           = \
    qetecsa_widget.ui \
    wsettings.ui

OTHER_FILES +=
