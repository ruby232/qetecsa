#include "modal.h"

Modal::Modal(QWidget *parent):
    QDialog(parent)
{
    this->setModal(true);
    this->setWindowFlags(Qt::Dialog);
    this->setLayout(new QHBoxLayout());

    l_message = new QLabel(this->message);
    this->layout()->addWidget(l_message);

    b_cancel = new QPushButton("Cancelar");
    this->layout()->addWidget(b_cancel);
    connect(b_cancel,SIGNAL(clicked()),this,SIGNAL(cancel()));



}

QString Modal::getMessage() const
{
    return message;
}

void Modal::setMessage(const QString &value)
{
    message = value;
    l_message->setText(message);
}
