#include "wsettings.h"
#include "ui_wsettings.h"

WSettings::WSettings(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::WSettings)
{
    ui->setupUi(this);
    ui->le_db_dir->setText(qApp->property("dirBD").toString());
}

WSettings::~WSettings()
{
    delete ui;
}

void WSettings::on_pb_Browser_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this);
           if (!fileName.isEmpty()){
               ui->le_db_dir->setText(fileName);
           }

}

void WSettings::on_buttonBox_accepted()
{
    qApp->setProperty("dirBD",ui->le_db_dir->text());

       QFile file(qApp->applicationDirPath() + "/data.dat");
       file.open(QIODevice::WriteOnly);
       QDataStream out(&file);
       out << ui->le_db_dir->text();
}

void WSettings::on_buttonBox_rejected()
{
    this->close();
}
