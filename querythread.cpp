#include "querythread.h"

QueryThread::QueryThread(QString query, QString db_dir, QObject *parent) :
    QThread(parent)
{
    this->query = query;
    this->db_dir = db_dir;

    if (QSqlDatabase::drivers().isEmpty()){
        emit(error("No database drivers found"));
    }

    db = QSqlDatabase::addDatabase("QSQLITE",db_dir);
    db.setConnectOptions("QSQLITE_OPEN_READONLY=1;QSQLITE_ENABLE_SHARED_CACHE=1");
    db.setDatabaseName(this->db_dir);

    if (!db.open()) {
        emit(error("Cannot open database"));
        qDebug()<< db.lastError().databaseText();
    }

    QHash <QString,QString> program;
    program["auto_vacuum"]= "FULL";//estaba NONE
    program["automatic_index"]= "ON";
    program["checkpoint_fullfsync"]= "OFF";
    program["foreign_keys"]= "OFF";
    program["fullfsync"]= "OFF";
    program["ignore_check_constraints"]= "OFF";
    program["journal_mode"]= "OFF";//DELETE
    program["journal_size_limit"]= "-1";
    program["locking_mode"]= "EXCLUSIVE";//NORMAL
    program["max_page_count"]= "1073741823";
    program["page_size"]= "16384";
    program["recursive_triggers"]= "OFF";
    program["secure_delete"]= "ON";
    program["synchronous"]= "OFF";//FULL
    program["temp_store"]= "FILE";
    program["user_version"]= "0";
    program["wal_autocheckpoint"]= "1000";
    program["cache_size"]= "163840";

    QHashIterator<QString, QString> i(program);
    while (i.hasNext()) {
        i.next();
        QSqlQuery(QString("PRAGMA %1 = %2").arg(i.key(),i.value()),db);
    }

}

void QueryThread::run()
{
   QSqlQueryModel *model = new QSqlQueryModel();
    model->setQuery(QSqlQuery(this->query, this->db));
    emit(result(model,0));
}

QString QueryThread::getQuery() const
{
    return query;
}

void QueryThread::setQuery(const QString &value)
{
    query = value;
}
