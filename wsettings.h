#ifndef WSETTINGS_H
#define WSETTINGS_H

#include <QDialog>
#include <QFileDialog>
#include <QDebug>

namespace Ui {
class WSettings;
}

class WSettings : public QDialog
{
    Q_OBJECT
    
public:
    explicit WSettings(QWidget *parent = 0);
    ~WSettings();
    
private slots:
    void on_pb_Browser_clicked();

    void on_buttonBox_accepted();

    void on_buttonBox_rejected();

private:
    Ui::WSettings *ui;
};

#endif // WSETTINGS_H
